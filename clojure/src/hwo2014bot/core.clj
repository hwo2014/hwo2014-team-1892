(ns hwo2014bot.core
  (:require [clojure.data.json :as json])
  (:use [aleph.tcp :only [tcp-client]]
        [lamina.core :only [enqueue wait-for-result wait-for-message]]
        [gloss.core :only [string]])
  (:gen-class))

;;;; various util functions

(defn index-by [pred coll]
  (first (first (filter #(pred (second %)) (zipmap (range) coll)))))

(defn index-of-min [coll]
  (first (first (sort-by second (zipmap (range) coll)))))

(defn maplist [f s]
  (loop [s s res []]
    (if (empty? s)
      res
      (recur (rest s) (conj res (f s))))))

(defn deg->radians [deg] (* Math/PI (/ deg 180)))

(defn sign [n] (if (< n 0) -1 1))

(defn find-max-value [pred start max-depth]
  (loop [n start d 2 depth 0]
    (if (<= max-depth depth)
      (if (pred n) n (- n (/ start d)))
      (if (pred n)
        (recur (+ n (/ start d)) (* d 2) (inc depth))
        (recur (- n (/ start d)) (* d 2) (inc depth))))))

(defn average [ns] (if (empty? ns) 0 (/ (reduce + ns) (count ns))))


;;; hwo util functions


(defn calc-speed [curp lastp pieces]
  (let [{{cur-ind :pieceIndex cur-dist :inPieceDistance} :piecePosition} curp
        {{last-ind :pieceIndex last-dist :inPieceDistance
          {slane :startLaneIndex lane :endLaneIndex} :lane} :piecePosition} lastp]
    (if (= cur-ind last-ind)
      (- cur-dist last-dist)
      (+ cur-dist (- (+ ((:dists (pieces last-ind)) lane)
                        (if (not= slane lane) 2 0))  ; not exact!!
                     last-dist)))))

;;; there is kind of a summation shortcut for this - add later
(defn slowing-distance [init-speed final-speed]
  (loop [speed init-speed s 0]
    (if (< speed final-speed)
      s
      (recur (- speed (* 0.02 speed)) (+ s speed)))))

(defn accel-distance
  ([init-speed final-speed] (accel-distance init-speed final-speed 1.0))
  ([init-speed final-speed throttle]
     (let [final-speed (min final-speed (* 10 (- throttle 0.01)))]
       (loop [speed init-speed s 0]
         (if (> speed final-speed)
           s
           (recur (+ speed (* 0.2 throttle) (* -0.02 speed)) (+ s speed)))))))


;;; simulator functions

(def init-physics-model {:throttle-const 0.2 :drag-const 0.02 :real-slip-const 0.45
                         :slip-const 1.0 :slip-drag-const -0.1 :anti-slip-const -0.075 :updated false})

(defn run-piece [lane piece physics-model target-velocity
                    init-v init-p init-angle init-a-v]
;  (println "simulating piece " (:ind piece))
  (loop [in-piece-dist init-p v init-v angle init-angle a-v init-a-v ticks 0]
    (let [dist (+ in-piece-dist v)
          throttle (if (< v target-velocity) 1.0 0.0)
          nv (+ v (- (* throttle (:throttle-const @physics-model))
                     (* v (:drag-const @physics-model))))
          nangle (+ angle a-v)
          a-v (+ a-v
                 (* (:slip-const @physics-model)
                    (let [slip
                          (case (:piece-type piece)
                            :straightaway 0
                            :left-turn (* -1 v v (/ 1 ((:radii piece) lane))
                                          (Math/cos (deg->radians angle)))
                            :right-turn (* v v (/ 1 ((:radii piece) lane))
                                           (Math/cos (deg->radians angle))))]
                      (if (> (Math/abs slip) 0.25) slip 0.0 )))
                 (* (:slip-drag-const @physics-model) a-v)
                 (* (:anti-slip-const @physics-model) v
                    (Math/sin (deg->radians angle))))]
;      (println (:ind piece ) ":  speed: " v " angle: " angle )
      (if (<= nv 0.1)
        'stopped   ; considering this a success
        (when #_(< (Math/abs  nangle) 60)
          (not (and (not= :straightaway (:piece-type piece))
                    (> v
                       (Math/sqrt (* ((:radii piece) lane)
                                     (:real-slip-const @physics-model))))))
          (if (> dist ((:dists piece) lane))
            {:dist (- dist ((:dists piece) lane)) :v nv :angle nangle :a-v a-v
             :ticks ticks}
            (recur dist nv nangle a-v (inc ticks) )
            ))))))

(defn run-sequence [lane pieces physics-model init-speed target-speed]
  (loop [pieces pieces in-piece-dist 0 v init-speed angle 0 a-v 0]
        (if (empty? pieces)
          'success
          (when-let [run-res
                     (run-piece lane (first pieces) physics-model target-speed
                                v in-piece-dist angle a-v)]
            (if (= run-res 'stopped)
              'stopped
              (let [{ndist :dist nv :v nangle :angle na-v :a-v nticks :ticks}
                    run-res]
                (recur (rest pieces) ndist nv nangle na-v)))))))

(defn run-simulator [pcind in-piece-dist v angle a-v lane
                     nticks pieces physics-model itinerary crash-angle]
  (loop [pcind pcind in-piece-dist in-piece-dist v v angle angle a-v a-v ticks 0]
    (let [dist (+ in-piece-dist v)
          throttle (if (< v (get-in @itinerary [pcind lane :speed-target]))
                     1.0
                     0.0)
          nv (+ v (- (* throttle (:throttle-const @physics-model))
                     (* v (:drag-const @physics-model))))
          npcind (if (> dist ((:dists (pieces pcind)) lane))
                   (mod (inc pcind) (count pieces))
                   pcind)
          ndist (if (not= npcind pcind)
                  (- dist ((:dists (pieces pcind)) lane))
                  dist)
          nangle (+ angle a-v)
          a-v (+ a-v
                 (* (:slip-const @physics-model)
                    (let [slip
                          (case (:piece-type (pieces pcind))
                            :straightaway 0
                            :left-turn (* -1 v v (/ 1 ((:radii (pieces pcind)) lane))
                                          (Math/cos (deg->radians angle)))
                            :right-turn (* v v (/ 1 ((:radii (pieces pcind)) lane))
                                           (Math/cos (deg->radians angle))))]
                      (if (> (Math/abs slip) 0.25) slip 0.0 )))
                 (* (:slip-drag-const @physics-model) a-v)
                 (* (:anti-slip-const @physics-model) v
                    (Math/sin (deg->radians angle))))]
;      (println pcind ": velocity:  " v " angle: " angle )
      (when #_(< (Math/abs  nangle) 60)
        (not (and (not= :straightaway (:piece-type (pieces pcind)))
                      (> v
                         (Math/sqrt (* ((:radii (pieces pcind)) lane)
                                       (:real-slip-const @physics-model))))))
        (if (= ticks nticks)
          'done
          (recur npcind ndist nv nangle a-v (inc ticks)))))))

;;;; itinerary functions 


(defn run-sequence-with-targets [run-w-targets lane physics-model init-speed]
  (loop [rt-pairs run-w-targets in-piece-dist 0 v init-speed angle 0 a-v 0]
    (if (empty? rt-pairs)
      'success
      (let [[[target-speed piece] & _] rt-pairs]
        (when-let [run-res
                   (run-piece lane piece physics-model
                              target-speed
                              v in-piece-dist angle a-v)]
          (if (= run-res 'stopped)
            'stopped
            (let [{ndist :dist nv :v nangle :angle na-v :a-v nticks :ticks}
                  run-res]
              (recur (rest rt-pairs) ndist nv nangle na-v))))))))



(defn set-speed-limits [init-run lane pieces physics-model]
  (let [res (take-while
             #(and (< (first (first %)) 20)
                   (= (:piece-type (second (first %))) :straightaway))
             (iterate (fn [targeted-run]
                        (let [max-speed-going-in
                              (find-max-value
                               #(run-sequence-with-targets
                                 targeted-run lane physics-model %) 10.0 7)
                              start-piece (second (first targeted-run))
                              new-piece (pieces (mod (dec (:ind start-piece))
                                                     (count pieces)))]
                          (conj targeted-run [max-speed-going-in new-piece])))
                      (map (fn [p] [0.0 p]) init-run)))
        res (last (take  res))
        n (- (count res) (count init-run))]
    (zipmap (map #(:ind (second %)) res) (map first (take n res)))))

(defn set-up-itinerary [itinerary pieces physics-model]
  (let [runs (partition-by :piece-type pieces)
        curved-runs (filter #(not= (:piece-type (first %)) :straightaway) runs)
        straight-runs (filter #(= (:piece-type (first %)) :straightaway) runs)
        lanes (range (count (:dists (first pieces))))]
    (doseq [ind (mapcat #(map :ind %) runs)]
      (doseq [lane lanes]
        (send itinerary assoc-in [ind lane :speed-target] 30.0)))
    (future (let [pcount (count pieces)]
              (doseq [[ind next-piece] (zipmap (range pcount) (rest (cycle pieces)))]
                (when (:switch next-piece)
                  (send itinerary assoc-in [ind :switch-ahead] (mod (inc ind) pcount)))))
            (doseq [run curved-runs]
              (doseq [lane lanes]
                (let [speed-target
                      (find-max-value
                       #(run-sequence lane run physics-model % %) 30.0 9)]
                  (doseq [ind (map :ind run)]
                    (send itinerary assoc-in [ind lane :speed-target] speed-target)))))
            (doseq [run curved-runs]
              (doseq [lane lanes]
                (let [speed-limits (set-speed-limits run lane pieces physics-model)]
;                  (println "setting speed limits: " speed-limits)
                  (doseq [[ind speed-limit] speed-limits]
                    (send itinerary assoc-in [ind lane :speed-target]
                          (min speed-limit
                               (get-in @itinerary [ind lane :speed-target])))))))
            (loop []
              (if (:updated @physics-model)
                (do
                  (send physics-model assoc :updated false)
                  (doseq [run curved-runs]
                    (doseq [lane lanes]
                      (let [speed-target
                            (find-max-value
                             #(run-sequence lane run physics-model % %) 30.0 9)]
                        (doseq [ind (map :ind run)]
                          (send itinerary assoc-in [ind lane :speed-target]
                                speed-target)))))
                  (doseq [run curved-runs]
                    (doseq [lane lanes]
                      (let [speed-limits (set-speed-limits run lane pieces physics-model)]
;                        (println "setting speed limits: " speed-limits)
                        (doseq [[ind speed-limit] speed-limits]
                          (send itinerary assoc-in [ind lane :speed-target]
                                (min speed-limit
                                     (get-in @itinerary [ind lane :speed-target])))))))
                  (recur))
                (do (Thread/sleep 100)
                    (recur)))))))


;;; explorer functions

(defn explorer-target-speeds [slip-const pieces]
  (let [runs (rest (partition-by #(= :straightaway (:piece-type %)) (cycle pieces)))
        runs (cons (first runs) (take-while #(not= % (first runs)) (rest runs)))]
    (into
     {}
     (mapcat (fn [run next-run]
               (if (= :straightaway (:piece-type (first run)))
                 (map (fn [piece] 
                        [(:ind piece)
                         (vec (map #(Math/sqrt (* slip-const %))
                                   (:radii (first next-run))))])
                      run)
                 (map (fn [piece]
                        [(:ind piece)
                         (vec (map #(Math/sqrt (* slip-const %)) (:radii piece)))])
                      run)))
             runs (rest (cycle runs))))))

(defn set-up-explorer [state]
  (let [slip-const (:real-slip-const  @(:physics-model state))]
    {:current-slip-const 0.0
     :max-angle 0.0
     :targetting-const slip-const
     :target-speeds (explorer-target-speeds slip-const (:pieces state))}))

;;; game init helper functions

(defn get-bot-index [botname data]
  (index-by #(= botname (get-in % [:id :name])) (get-in data [:race :cars])))

(defn process-pieces [data]
  (let [lanes (vec (map :distanceFromCenter
                        (sort-by :index
                                 (get-in data [:race :track :lanes]))))
        pieces (get-in data [:race :track :pieces])
        pieces (map (fn [p i] (assoc p :ind i)) pieces (range))
        pieces (map (fn [piece]
                      (assoc piece :piece-type (cond (:length piece) :straightaway
                                                     (< (:angle piece) 0) :left-turn
                                                     :else :right-turn)))
                    pieces)
        pieces (map (fn [piece]
                      (if (= (:piece-type piece) :straightaway)
                        piece
                        (assoc piece :radii
                               (vec (map #(+ (:radius piece)
                                             (* (case (:piece-type piece)
                                                      :left-turn 1
                                                      :right-turn -1)
                                                    %))
                                         lanes)))))
                    pieces)
        pieces (map (fn [piece]
                      (assoc piece :dists
                             (vec (map (fn [lane]
                                         (if (:length piece)
                                           (:length piece)
                                           (* (Math/abs (deg->radians (:angle piece)))
                                              ((:radii piece) lane))))
                                       (range (count lanes))))))
                    pieces)]
    (vec (sort-by :ind pieces))))

(defn set-up-switch-map [switch-map pieces]
  "creates a table keyed by switch pieces giving length of track for each lane
   until next switch"
  (let [pieces (take (count pieces) (drop-while (complement :switch) (cycle pieces)))
        runs (partition 2 (partition-by :switch pieces))
        runs (mapcat (fn [[a b]] (concat (map vector (butlast a)) [[(last a) b]]))
                     runs)   ; to deal with consecutive switch pieces
        ]
    (doseq [[{ind :ind} pcs] runs]
      (send switch-map assoc ind (vec (apply map + (map :dists pcs)))))))


(def init-state  {:bot-name "Herb"  ; testing default
                  :msgs []})

(defn measure-physics-constants [state]
  (let [samples (drop 4 (get-in state [:dashboard :physics-data]))
        cs (map (fn [[t1 v1 a1] [t2 v2 a2]]
                  (let [d (- (* t2 v1) (* t1 v2))]
                    (when (not= d 0.0)
                      (/ (- (* t1 a2) (* t2 a1)) d))))
                samples (rest samples))
        drag-const (average (remove nil? cs))
        ts (map (fn [[t v a]] (when (not= t 0.0)
                                (/ (+ a (* drag-const v)) t)))
                      samples)
        throttle-const (average (remove nil? ts))]
    (println "UPDATED PHYSICS CONSTANTS:  throttle: " throttle-const
             "  drag: " drag-const)
    (send (state :physics-model) merge
          {:throttle-const throttle-const :drag-const drag-const
           :updated true :straightaway-constants-updated true})))

;;; only thing to be done is to get a sequence and correctly deduce that
;;; it peaked at a certain angle

(defn analyze-series [state]
  (let [samples (take-last 5 (get-in state [:dashboard :physics-data]))
        angles (map #(Math/abs (nth % 3)) samples)
        pcinds (map #(nth % 5) samples)
        lanes (map #(nth % 6) samples)
        mx (apply max angles)]
    (when (and (apply = pcinds) (= mx (nth angles 2)))
      {:angle (Math/abs (nth angles 2)) :speed (nth (nth samples 2) 1)
       :radius (get-in state [:pieces (nth pcinds 2) :radii (nth lanes 2)])})))

(defn measure-slip-constant [state]
  (if (> (get-in state [:explorer :max-angle]) 55)
    (do #_(send (:physics-model state) merge
              {:real-slip-const (get-in state [:explorer :current-slip-const])
               :updated true})
        (dissoc state :explorer))
    (if (= (get-in state [:pieces (get-in state [:dashboard :pcind]) :piece-type])
           :straightaway)
      state
      (let [explorer (:explorer state)
            dashboard (:dashboard state)]
        (if (and (= (:max-angle explorer) 0.0) (= (:angle dashboard) 0.0))
          (do (send (:itinerary state) assoc-in
                    [(:pcind dashboard) (:lane dashboard) :speed-target]
                    (+ (:speed dashboard) 0.5))
              (assoc-in state
                        [:explorer :target-speeds (:pcind dashboard) (:lane dashboard)]
                        (+ (:speed dashboard) 0.5)))
          (if-let [{angle :angle v :speed r :radius}
                   (analyze-series state)]
            (if (> angle (:max-angle explorer))
              (let [new-slip-const (/ (* v v ) r)]
                (println "MEASURING SLIP CONSTANT   " v )
                (println "New max angle: " angle)
                (println "new const: " new-slip-const)
                (if (and (> new-slip-const (:targetting-const explorer)))
                  (let [v (+ v (Math/pow (Math/cos (deg->radians angle)) 4))
                        new-target (max
                                    (/ (* v v) r)
                                    (:targetting-const explorer))]
                    (println "INCREASING TARGET SPEEDS")
                    (assoc state :explorer
                           (merge explorer
                                  {:max-angle angle
                                   :current-slip-const new-slip-const
                                   :targetting-const new-target
                                   :target-speeds
                                   (explorer-target-speeds new-target
                                                           (:pieces state))})))
                  (assoc state :explorer
                         (merge explorer
                                {:max-angle angle :current-slip-const new-slip-const}))))
              state)
            state))))))

(defn init-dashboard []
  {:notices {}, :physics-data [], :speed 0.0 :ticks 0
   :last-position {:angle 0.0
                   :piecePosition {:pieceIndex 0 :inPieceDistance 0.0
                                   :lane {:startLaneIndex 0 :endLaneIndex 0}}}})

(defn update-dashboard [msg state]
  (let [data (get-in msg [:data (:bot-ind state)])
        last-position (get-in state [:dashboard :last-position])
        {angle :angle {pcind :pieceIndex {lane :endLaneIndex}:lane}:piecePosition} data
        {langle :angle {lpcind :pieceIndex}:piecePosition} last-position
        speed (calc-speed data last-position (:pieces state))
        ticks (get-in state [:dashboard :ticks])
        nv {:angle angle :a-v (if (= speed 0.0) 0.0 (- langle angle))
            :lane lane :pcind pcind :speed speed :ticks (inc ticks)}
        ;; stop updating when constants established
        state (update-in state [:dashboard :physics-data] conj
                         [(get-in state [:dashboard :throttle]) ; 0
                          (get-in state [:dashboard :speed])    ; 1
                          (- speed (get-in state [:dashboard :speed])) ; 2
                          (get-in state [:dashboard :angle])  ; 3
                          (get-in state [:dashboard :a-v])  ; 4
                          (get-in state [:dashboard :pcind]) ; 5 
                          (get-in state [:dashboard :lane])]) ; 6
        state (assoc state :dashboard (merge (:dashboard state) nv))
;        state (if (:explorer state) (measure-slip-constant state) state)
        state (if (not= pcind lpcind)
                (do (println "in piece " pcind)
                    (println "speed: " (:speed nv) " angle: " angle
                             " angular-velocity: " (:a-v nv) " lane: " lane
                             " " (get-in state [:dashboard :notices]))
                    (when (and (= pcind 1)
                               (not (:straightaway-constants-updated  ; add this to
                                     @(:physics-model state))))     ; dashboard
                      (measure-physics-constants state))
                    (assoc-in state [:dashboard :notices] {}))
                state)
        state (assoc-in state [:dashboard :last-position] data)]
     state))




;;; handle msg functions

(defmulti handle-msg :msgType)


(defn crash-predictor [state]
  (let [{v :speed pcind :pcind lane :lane a-v :a-v angle :angle
         {{in-piece-dist :inPieceDistance} :piecePosition} :last-position}
        (:dashboard state)]
    (when (not (run-simulator pcind in-piece-dist v angle a-v lane 10
                              (:pieces state) (:physics-model state)
                              (:itinerary state)
                              50))    ;;;; changed angle
      (println "sharp turn ahead in section " pcind)
      true)))

(defn target-throttle [speed-target state]
  (let [physics-model @(:physics-model state)
        throttle
        (min 1.0 (max 0.0 (* (/ (:throttle-const physics-model)
                                (:drag-const physics-model))
                             speed-target)))]
    [{:msgType "throttle" :data throttle}
     (assoc-in state [:dashboard :throttle] throttle)]))

(defn throttle-msg [speed-target state]
  (let [speed (get-in state [:dashboard :speed])
        pcind (get-in state [:dashboard :pcind])
        lane (get-in state [:dashboard :lane])]   ; doesn't seem to do much
    (if false ;(:explorer state)
      (let [speed-target (get-in state [:explorer :target-speeds pcind lane])]
        (if (> speed speed-target)
          (target-throttle 0.0 state)
          (target-throttle 1.0 state)))
      (if-let [st (get-in @(:itinerary state) [pcind :upcoming-speed-target])]
        (let [pp (get-in state [:dashboard :last-position
                                :piecePosition :inPieceDistance])
              dist (get-in state [:pieces pcind :dists lane])
              dist-remaining (- dist pp)]
          (if (< (slowing-distance speed st) dist-remaining)  ; should calculate once
            (if (> speed speed-target)               ; and post to notices
              (target-throttle 0.0 state)
              (target-throttle 1.0 state))
            (if (> speed st)
              (target-throttle 0.0 state)
              (target-throttle 1.0 state))
            ))
        (if (> speed speed-target)
          (target-throttle 0.0 state)
          (target-throttle 1.0 state))))))

(defn switch-lane [swpcind state]
  (let [lane (get-in state [:dashboard :lane])
        best-lane (first
                   (first
                    (sort-by
                     second
                     (filter #(#{lane (dec lane) (inc lane)} (first %))
                             (zipmap (range) (@(:switch-map state) swpcind))))))]
    (case (- lane best-lane)
      1  [{:msgType "switchLane" :data "Left"}
          (assoc-in state [:dashboard :notices :switched] true)]
      -1 [{:msgType "switchLane" :data "Right"}
          (assoc-in state [:dashboard :notices :switched] true)]
      [{:msgType "ping" :data "ping"}
          (assoc-in state [:dashboard :notices :switched] true)])))

(defmethod handle-msg "carPositions" [msg state]
  (let [state (update-dashboard msg state)
        pcind (get-in state [:dashboard :pcind])
        lane (get-in state [:dashboard :lane])
        speed-target (get-in @(:itinerary state) [pcind lane :speed-target])
        ]
    (if (crash-predictor state)
      (target-throttle 0.0 state)
      (if (get-in @(:itinerary state) [pcind :boost-available])
        (do (send (:itinerary state) assoc-in [pcind :boost-available] false)
            [{:msgType "turbo" :data "whooooooo"} state])
        (if (< (get-in state [:dashboard :speed])
               (* 0.5 speed-target))
          (throttle-msg speed-target state)
          (let [swpcind (get-in @(:itinerary state) [pcind :switch-ahead])]
            (if (and swpcind
                     (not (get-in state [:dashboard :notices :switched])))
              (switch-lane swpcind state)
              (throttle-msg speed-target state))))))))


(defmethod handle-msg "turboAvailable" [msg state]

  [{:msgType "ping" :data "ping"} state])

(defmethod handle-msg "gameInit" [msg state]
  (let [data (:data msg)
        pieces (process-pieces data)
        itinerary (agent {})
        switch-map (agent {})
        physics-model (agent init-physics-model)
        state (assoc state
                :bot-ind (get-bot-index (:bot-name state) data)
                :pieces pieces
                :itinerary itinerary
                :switch-map switch-map
                :physics-model physics-model
                :dashboard (init-dashboard)
                :current-throttle 0.0)
        state (assoc state :explorer (set-up-explorer state))]
    (set-up-itinerary itinerary pieces physics-model)
    (set-up-switch-map switch-map pieces)
    [{:msgType "ping" :data "ping"} state]
))

(defmethod handle-msg "crash" [msg state]
  (let [car-name (get-in msg [:data :name])]
    (when (= car-name (:bot-name state))
       (println "I crashed"))
    
    [{:msgType "ping" :data "ping"} state]
    ))

(defmethod handle-msg "yourCar" [msg state]
  [{:msgType "ping" :data "ping"}
   (assoc state :bot-name (get-in msg [:data :name]))])

(defmethod handle-msg :default [msg state]
  [ {:msgType "ping" :data "ping"} state])


;;; client functions

(defn- json->clj [string]
  (json/read-str string :key-fn keyword))

(defn send-message [channel message]
  (enqueue channel (json/write-str message)))

(defn read-message [channel]
  (json->clj
    (try
      (wait-for-message channel)
      (catch Exception e
        (println (str "ERROR: " (.getMessage e)))
        ;;;;; arrrgggghhhh  - my repl!!!
        (System/exit 1)))))

(defn connect-client-channel [host port]
  (wait-for-result
   (tcp-client {:host host,
                :port port,
                :frame (string :utf-8 :delimiters ["\n"])})))

(defn log-msg [msg]
  (case (:msgType msg)
    "join" (println "Joined")
    "gameStart" (println "Race started")
    "crash" (println "Someone crashed")
    "gameEnd" (println "Race ended")
    "error" (println (str "ERROR: " (:data msg)))
    :noop))

(defn next-logfile-index [track-name]
   (let [re (re-pattern (str track-name "-(\\d+)\\.log"))
         filenames (clojure.string/join
                    " "
                    (map #(.getName %)
                         (filter #(.isFile %)
                                 (file-seq (clojure.java.io/file "log")))))
         matches (re-seq re filenames)]
     (if (empty? matches)
       1
       (inc (apply max (map #(read-string (second %)) matches))))))

(defn log-to-file [msgs]
  (let [init-msg (first (filter #(= "gameInit" (:msgType %)) msgs))
        track-name (get-in init-msg [:data :race :track :name])
        race-num (next-logfile-index track-name)
        log-file (str "log/" track-name "-" race-num ".log")]
    (with-open [log (clojure.java.io/writer log-file :append true)]
      (doseq [msg msgs] (.write log (json/write-str msg)) (.write log "\n")))))

(defn game-loop [channel state]
  (let [msg (read-message channel)]
    (log-msg msg)
    (let [[outmsg state] (handle-msg msg state)
          state (assoc state :msgs (conj (:msgs state) msg outmsg))]
      (send-message channel outmsg)
      (recur channel state))))

(defn -main[& [host port botname botkey]]
  (let [channel (connect-client-channel host (Integer/parseInt port))]
    (send-message channel
                  {:msgType "join" :data {:name botname :key botkey}})
    (game-loop channel (assoc init-state :bot-name botname))))


(defn repl-safe-read-message [channel]
  (json->clj
    (try
      (wait-for-message channel)
      (catch Exception e
        (println (str "ERROR: " (.getMessage e)))))))

(defn logged-game-loop [channel state]
  (let [msg (read-message channel)]
    (log-msg msg)
    (let [[outmsg state] (handle-msg msg state)
          state (assoc state :msgs (conj (:msgs state) msg outmsg))]
      (send-message channel outmsg)
      (if (= (:msgType msg) "gameEnd")
        (log-to-file (:msgs state))
        (recur channel state)))))

(defn main [trackname]
  (let [[host port botname botkey trackname]
        ["prost.helloworldopen.com" "8091" "Herb" "CiuQ3pAVG+MMKg" trackname]]
    (let [channel (connect-client-channel host (Integer/parseInt port))]
      (send-message channel
                    {:msgType "createRace"
                     :data {:botId
                            {:name botname :key botkey}
                            :trackName trackname :password "spooky"
                            :carCount 1}})
      (send-message channel {:msgType "joinRace"
                             :data {:botId
                                    {:name botname :key botkey}
                                    :trackName trackname :password "spooky"}})
    (logged-game-loop channel (assoc init-state :bot-name botname)))))




(defn log->init-msg [track-name]
   (let [re (re-pattern (str track-name "-\\d+\\.log"))
         filenames (clojure.string/join
                    " "
                    (filter #(.isFile %)
                            (file-seq (clojure.java.io/file "log"))))
         matches (re-seq re filenames)
         filename (first (map #(str "log/" %) matches))
         data (map json->clj (clojure.string/split-lines (slurp filename)))
         data (map (fn [[a b]] (assoc a :throttle (:data b)))
                   (partition 2 data))
         game-init (first (filter #(= "gameInit" (:msgType %)) data))]
     game-init))


(defn simulator-test [track-name]
  (let [data (:data (log->init-msg track-name))
        pieces (process-pieces data)
        itinerary (agent {})]
    (set-up-itinerary itinerary pieces init-physics-model)
    (Thread/sleep 1000)
    pieces))


(defn pos->pcind [pos]
  (get-in pos [:piecePosition :pieceIndex]))

(defn car-angle [pos] (get-in pos [:angle]))

(defn car-lane [pos] (get-in pos [:piecePosition :lane :startLaneIndex]))

(defn parse-log [logfile]
  (let [data (map json->clj (clojure.string/split-lines (slurp logfile)))
        data (map (fn [[a b]] (assoc-in a [:data 0 :reply] b)) (partition 2 data))
        game-init (first (filter #(= "gameInit" (:msgType %)) data))
        pieces (process-pieces (:data game-init))
        data (filter #(= "carPositions" (:msgType %)) data)
        data (sort-by :gameTick (filter :gameTick data))
        data (map #(get-in % [:data 0]) data)
        triplets (partition 3 1 data)
        runs (partition-by (fn [[_ p _]] (:piece-type (pieces (pos->pcind p))))
                           triplets)
        runs (map (fn [run]
                    (filter #(not= (:v1 %) 0)  ; filter crashes
                            (map (fn [[p1 p2 p3]]
                                   { :v1 (calc-speed p2 p1 pieces)
                                    :phi1 (- (car-angle p2) (car-angle p1))
                                    :phi2 (- (car-angle p3) (car-angle p2))
                                    :theta1 (car-angle p2)
                                    :theta2 (car-angle p3)
                                    :acc (- (calc-speed p3 p2 pieces)
                                            (calc-speed p2 p1 pieces))
                                    :type (:piece-type (pieces (pos->pcind p2)))
                                    :pcind (pos->pcind p2)
                                    :lane (car-lane p2)
                                    :init-pos (get-in p2 [:piecePosition 
                                                          :inPieceDistance])
                                    :radius (when (:radius (pieces (pos->pcind p2)))
                                              ((:radii (pieces (pos->pcind p2)))
                                               (car-lane p2)))
                                    :throttle (if (= (get-in p2 [:reply :msgType])
                                                     "throttle")
                                                (get-in p2 [:reply :data])
                                                0.0)})
                                 run)))
                  runs)]
    runs))




(defn log->runs [logfile]
  (let [data (map json->clj (clojure.string/split-lines (slurp logfile)))
        data (map (fn [[a b]] (assoc-in a [:data 0 :reply] b)) (partition 2 data))
        game-init (first (filter #(= "gameInit" (:msgType %)) data))
        pieces (process-pieces (:data game-init))
        runs (take (count (partition-by :piece-type pieces))
                   (partition-by :piece-type (cycle pieces)))]
    runs))


(defn sq [x] (* x x))

(defn standard-deviation [ns]
  (let [mean (average ns)]
    (Math/sqrt (average (map #(sq (- % mean)) ns)))))
(defn clip-number [n d] (let [b (Math/pow 10 d)] (/ (int (* b n)) b)) )

(defn generate-time-series [{v :v1 ipp :init-pos angle :theta1 a-v :phi1 lane :lane}
                            run throttle-commands physics-model]
  (loop [run run
         in-piece-pos ipp
         v v angle angle a-v a-v
         throttle-commands throttle-commands
         time-series []]
    (let [dist (+ in-piece-pos v)
          nv (+ v (- (* (or (first throttle-commands) 0.0)
                        (:throttle-const physics-model))
                     (* v (:drag-const physics-model))))
          nrun (if (> dist ((:dists (first run)) lane))
                 (rest run)
                 run)
          ndist (if (not= nrun run)
                  (- dist ((:dists (first run)) lane))
                  dist)
          nangle (+ angle a-v)
          a-v (+ a-v
                 (* (:slip-const physics-model)
                    (let [slip
                          (case (:piece-type (first run))
                            :straightaway 0
                            :left-turn (* -1 v v (/ 1 ((:radii (first run)) lane))
                                          (Math/cos (deg->radians angle)))
                            :right-turn (* v v (/ 1 ((:radii (first run)) lane))
                                           (Math/cos (deg->radians angle))))]
                      (if (> (Math/abs slip) 0.25) slip 0.0 )))
                 (* (:slip-drag-const physics-model) a-v)
                 (* (:anti-slip-const physics-model) v
                    (Math/sin (deg->radians angle))))]
         (println (:ind (first nrun)))
      (if (empty? nrun)
        time-series
        (recur nrun ndist nv nangle a-v
               (next throttle-commands) (conj time-series nangle))))))

(defn compare-series [s1 s2] (reduce + (map (fn [a b] (sq (- a b))) s1 s2)))

(defn gaussian [variance]
  (let [rand1 (rand)
        rand1 (* -2 (Math/log rand1))
        rand2 (* 2 Math/PI (rand))
        sign (if (= 0 (rand-int 2)) 1 -1)]
    (* sign (Math/sqrt (* variance rand1)) (Math/cos (rand)))))

(defn vary-physics [physics variance]
  (let [const (case (rand-int 3) 0 :slip-const 1 :slip-drag-const 2 :anti-slip-const)]
    (assoc physics const (+ (get physics const) (gaussian variance)))))

(defn show-time-series [logfile run-ind physics-model]
  (let [run (nth (log->runs logfile) run-ind)
        data (nth (parse-log logfile) run-ind)
        throttle-commands (map :throttle data)
        init-state (first data)]
    (generate-time-series
     init-state run throttle-commands physics-model)))

(defn find-constants [logfile run-ind]
  (let [run (nth  (log->runs logfile) run-ind)
        data (nth (parse-log logfile) run-ind)
        time-series (map :theta1 data)
        throttle-commands (map :throttle data)
        init-state (first data)
        init-error (compare-series
                    time-series
                    (generate-time-series
                     init-state run throttle-commands init-physics-model))]
    (println init-error)
    (loop [temp 10.0 physics init-physics-model prev-error init-error]
      (if (< temp 0)
        (do (println "error: " init-error) physics)
        (let [nphysics (vary-physics physics temp)
              nerror (compare-series
                      time-series
                      (generate-time-series
                       init-state run throttle-commands nphysics))]
          (if (< nerror prev-error)
            (recur (- temp 1) nphysics nerror)
            (recur (- temp 1) physics prev-error)))))))



